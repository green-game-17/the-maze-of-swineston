extends Node2D


onready var setup_sprite = $SETUP
onready var normal_sprite = $NORMAL
onready var active_sprite = $ACTIVE
onready var destroyed_sprite = $DESTROYED

var type_id: int
var tiles: Array
var durability: int


func _ready():
	$SetupTimer.connect('timeout', self, '_on_setup_timeout')
	$TriggeredTimer.connect('timeout', self, '_on_triggered_timeout')
	set_position(STATE.get_world_position(tiles[0]))

	var sprites = DATA.get_sprites(type_id)
	normal_sprite.set_texture(sprites.normal)
	active_sprite.set_texture(sprites.active)
	destroyed_sprite.set_texture(sprites.destroyed)

	set_position(Vector2(
		tiles[0].x * DATA.GRID_PX_SIZE + DATA.TILE_PX_SIZE / 2,
		tiles[0].y * DATA.GRID_PX_SIZE + DATA.TILE_PX_SIZE / 2
	))


func set_data(data: Dictionary):
	type_id = data.type_id
	tiles = data.tiles
	durability = DATA.get_durability(data.type_id)


func reduce_durability(amount: int):
	durability -= amount
	normal_sprite.hide()
	active_sprite.show()
	$TriggeredTimer.start()


func is_active() -> bool:
	return durability > 0


func disarmed():
	durability = 0
	normal_sprite.hide()
	destroyed_sprite.show()


func _on_setup_timeout():
	setup_sprite.hide()
	normal_sprite.show()


func _on_triggered_timeout():
	active_sprite.hide()
	if durability <= 0:
		disarmed()
	else:
		normal_sprite.show()


func destroy():
	STATE.change_life_force_count(DATA.get_life_force(type_id) / 5) # intentional int division
	queue_free()
