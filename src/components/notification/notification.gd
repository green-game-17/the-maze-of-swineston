extends Node2D


func _ready():
	pass # Replace with function body.


func set_data(text: String, show_icon: bool = true, color: Color = Color.crimson):
	$VBoxContainer/Label.set_text(text)
	$VBoxContainer/Label.add_color_override("font_color", color)
	if not show_icon:
		$VBoxContainer/Icon.hide()


func _process(delta):
	position.y -= delta * 500.0
	modulate.a *= 0.995

	if modulate.a < 0.8:
		modulate.a *= 0.9

	if modulate.a < 0.001:
		hide()
		queue_free()
