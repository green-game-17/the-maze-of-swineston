extends Node2D


const CAMERA_SPEED = 350
const SPAWN_TIME = 10.0

onready var camera = $Camera
onready var spawn_timer = $SpawnTimer
onready var adventurers = $Adventurers
onready var music_stage_1 = $MusicStage1
onready var music_stage_2 = $MusicStage2
onready var music_stage_3 = $MusicStage3

var adventurer_scene = load('res://components/adventurer/adventurer.tscn')
var monster_scene = load('res://components/monster/monster.tscn')

var current_stage = 1
var new_stage = 1
var music_stages = []
var shifting_volume = 0


func _ready():
	STATE.reset()
	STATE.reset_maze()

	spawn_timer.connect('timeout', self, '_on_spawn_timeout')
	STATE.connect('no_adventurer_alive', self, '_on_no_adventurers_alive')
	EVENTS.connect('spawn_new_adventurer', self, '__spawn_new_adventurer')

	EVENTS.connect('adventurer_reached_goal', self, '_on_reached_goal')
	STATE.connect('souls_count_changed', self, '_on_souls_changed')

	spawn_timer.set_wait_time(SPAWN_TIME)

	music_stage_1.connect('finished', self, '_on_music_finished', [1])
	music_stage_2.connect('finished', self, '_on_music_finished', [2])
	music_stage_3.connect('finished', self, '_on_music_finished', [3])
	music_stages = [music_stage_1, music_stage_2, music_stage_3]


func _input(_event):
	if Input.is_action_just_pressed('camera_zoom_in'):
		camera.zoom.x *= 0.9
		camera.zoom.y *= 0.9
	elif Input.is_action_just_pressed('camera_zoom_out'):
		camera.zoom.x *= 1.2
		camera.zoom.y *= 1.2


func _physics_process(delta):
	if music_stages.size() > 0:
		var max_length = STATE.get_dist_start_goal()
		var closest_adv = STATE.get_closest_adventurer(STATE.get_goal_tile())
		var dist = max(closest_adv.distance, 0)
		if closest_adv.distance == -1:
			new_stage = 1
		else:
			new_stage = 4 - clamp(floor(dist / (max_length / 3)) + 1, 1, 3)
			
		if new_stage != current_stage:
			shifting_volume += 1
			if new_stage < current_stage:
				if shifting_volume >= 20:
					music_stages[current_stage - 1].set_volume_db(-20.0)
					shifting_volume = 0
					current_stage = new_stage
				else:
					music_stages[current_stage - 1].set_volume_db(0.0 - shifting_volume)
			elif new_stage > current_stage:
				if shifting_volume >= 20:
					music_stages[new_stage - 1].set_volume_db(0.0)
					shifting_volume = 0
					current_stage = new_stage
				else:
					music_stages[new_stage - 1].set_volume_db(-20.0 + shifting_volume)
	
	if Input.is_action_pressed("camera_up"):
		camera.position.y -= camera.zoom.y * CAMERA_SPEED * delta
	if Input.is_action_pressed("camera_down"):
		camera.position.y += camera.zoom.y * CAMERA_SPEED * delta
	if Input.is_action_pressed("camera_left"):
		camera.position.x -= camera.zoom.x * CAMERA_SPEED * delta
	if Input.is_action_pressed("camera_right"):
		camera.position.x += camera.zoom.x * CAMERA_SPEED * delta


func _on_reached_goal():
	SCENE.goto_scene('fail')


func _on_souls_changed(count):
	if count >= DATA.TARGET_SOULS_COUNT:
		SCENE.goto_scene('win')


func _on_no_adventurers_alive():
	spawn_timer.stop()
	_on_spawn_timeout()


func _on_spawn_timeout():
	var new_class_id = STATE.get_random_class()
	__spawn_new_adventurer(new_class_id)
	spawn_timer.start()


func _on_music_finished(stage: int):
	if current_stage == stage or new_stage == stage:
		music_stage_1.play()
		music_stage_2.play()
		music_stage_3.play()


func __spawn_new_adventurer(new_class_id: int):
	var new_adventurer = adventurer_scene.instance()
	new_adventurer.set_data(new_class_id)
	adventurers.add_child(new_adventurer)
	STATE.change_adventurers_alive(1)
