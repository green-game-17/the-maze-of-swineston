extends CanvasLayer


func _ready():
	$GameUi/HBoxContainer/Empty.connect('pressed', self, '_on_empty_pressed')


func _on_empty_pressed():
	EVENTS.emit_signal('non_ui_mouse_click')
