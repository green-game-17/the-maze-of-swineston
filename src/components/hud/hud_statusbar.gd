extends Control


onready var souls_label = $Margin/Container/SoulCountContaiiner/VBoxContainer/HBoxContainer/Label
onready var souls_progress = $Margin/Container/SoulCountContaiiner/VBoxContainer/ProgressBar
onready var life_force_label = $Margin/Container/LifeForceCountContaiiner/Label
onready var maze_depth_label = $Margin/Container/MazeDepthContaiiner/Label
onready var time_label = $Margin/Container/TimeContainer/Label


func _ready():
	STATE.connect('souls_count_changed', self, '_on_souls_count_changed')
	STATE.connect('life_force_count_changed', self, '_on_life_force_count_changed')
	STATE.connect('maze_extended', self, '_on_maze_extended')
	STATE.connect('playtime_advanced', self, '_on_playtime_advanced')

	_on_souls_count_changed(STATE.get_souls_count())
	_on_life_force_count_changed(STATE.get_life_force_count())
	_on_maze_extended()


func _on_souls_count_changed(count):
	souls_label.set_text('%d' % count)
	souls_progress.set_value( max(1, float(count*100) / float(DATA.TARGET_SOULS_COUNT)) )


func _on_life_force_count_changed(count):
	life_force_label.set_text('%d' % count)


func _on_maze_extended():
	var depth = STATE.get_goal_tile().y + 1
	maze_depth_label.set_text('%d' % depth)


func _on_playtime_advanced(time):
	var hours = int(time / (60 * 60))
	var minutes = int((time - hours*60*60) / 60)
	var seconds = int(time - hours*60*60 - minutes * 60)

	time_label.set_text('%02d:%02d:%02d' % [hours, minutes, seconds])
