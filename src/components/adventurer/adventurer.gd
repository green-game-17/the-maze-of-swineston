extends Node2D

class_name Adventurer


const MOVING_SPEED = 500
const ROGUE_MOVING_SPEED = 750

onready var up_sprite = $UP
onready var right_sprite = $RIGHT
onready var down_sprite = $DOWN
onready var left_sprite = $LEFT
onready var thinking_sprite = $Thinking
onready var fighting_sprite = $Fighting
onready var disarming_sprite = $Disarming
onready var seeing_trap_sprite = $SeeingTrap

var class_id: int
var strength: int
var next_tile: Vector2
var old_tile: Vector2
var orientation_vec: Vector2
var adventurer_group: AdventurerGroup
var pos_offset = Vector2(0, 0) # position offset, so that the group can be displayed correctly
var target_path = []
var known_traps = [] # includes trap_instances and their position
var open_paths = []
var astar_node = AStar2D.new()
var searching_new_path = false
var is_stuck = false
var attacking_monsters = []
var monster_ready = false

var is_in_adventurer_group = false

var possible_directions = []
var icon_offset = 0


func _ready():
	STATE.connect('maze_wall_moved', self, '_on_maze_wall_moved')
	STATE.connect('maze_trap_placed', self, '_on_maze_trap_placed')
	STATE.connect('monster_moved', self, '_on_monster_moved')
	STATE.connect('maze_extended', self, 'set_new_reserved_space')
	EVENTS.connect('adventurer_moving', self, '_on_adventurer_moving')
	$ThinkingTimer.connect('timeout', self, '_on_thinking_timeout')
	$FightingTimer.connect('timeout', self, '_on_fighting_timeout')
	$DisarmTimer.connect('timeout', self, '_on_disarm_timeout')
	$SeeingTimer.connect('timeout', self, '_on_seeing_timeout')
	old_tile = STATE.get_start_tile() + Vector2(0, -1)
	set_position(
		STATE.get_world_position(
			old_tile
		) + pos_offset
	)
	next_tile = STATE.get_start_tile()
	orientation_vec = next_tile - old_tile
	astar_node.add_point(astar_node.get_available_point_id(), next_tile)
	var sprites = DATA.get_sprites(class_id)
	up_sprite.set_texture(sprites.up)
	right_sprite.set_texture(sprites.right)
	down_sprite.set_texture(sprites.down)
	left_sprite.set_texture(sprites.left)
	__refresh_sprite()


func set_data(new_class_id: int):
	class_id = new_class_id
	strength = DATA.get_strength(class_id)
	if class_id == ID.TEAMLEADER:
		adventurer_group = AdventurerGroup.new()
		adventurer_group.set_leader(self)
	set_new_reserved_space()


func set_new_reserved_space():
	var tile_count = STATE.get_tile_count()
	if tile_count > 64:
		astar_node.reserve_space(tile_count)


func _on_maze_trap_placed(trap_tiles: Array):
	var is_in_reach = false
	for trap_tile in trap_tiles:
		if not __tile_in_reach(trap_tile) in [-1, 3]:
			is_in_reach = true
			break
	
	if is_in_reach:
		for trap_tile in trap_tiles:
			var tile_index = __tile_in_astar(trap_tile)
			if tile_index != -1:
				astar_node.set_point_disabled(tile_index)
			else:
				var trap_tile_info = STATE.get_tile_info(trap_tile)
				known_traps.append({ tile = trap_tile, ref = trap_tile_info.tile })


func _on_maze_wall_moved(from_tiles: Array, to_tiles: Array):
	if is_in_adventurer_group:
		return
	
	if is_stuck:
		for tile in from_tiles:
			if next_tile == tile:
				is_stuck = false
				break
	
	if next_tile in to_tiles and old_tile in to_tiles:
		var current_pos = get_position() - pos_offset
		if ((current_pos - STATE.get_world_position(next_tile)).length_squared() >
			(current_pos - STATE.get_world_position(old_tile)).length_squared()):
			next_tile = old_tile
			if class_id == ID.TEAMLEADER:
				adventurer_group.remove_latest_tile()
				if not adventurer_group.is_leading_group:
					adventurer_group.split_up()
		elif is_in_adventurer_group:
			var leading_group = adventurer_group.get_leading_group()
			var own_pg_index = leading_group.partner_groups.find(adventurer_group)
			if leading_group.partner_groups.size() > own_pg_index + 1:
				var new_leading_group = leading_group.partner_groups[own_pg_index + 1]
				new_leading_group.remove_latest_tile()
				new_leading_group.split_up()


func _on_monster_moved(_m_old_tile: Vector2, m_new_tile: Vector2, monster):
	var is_in_reach = __tile_in_reach(m_new_tile)
	if is_in_reach == 0:
		if m_new_tile == next_tile + orientation_vec and not monster in attacking_monsters:
			attacking_monsters.append(monster)
			monster.attacking_adventurer = true
	elif is_in_reach in [1, 2, 3] and not monster in attacking_monsters:
		attacking_monsters.append(monster)
		monster.attacking_adventurer = true
	# TODO: run away: polish


# 0 = straight ahead, 1 = right, 2 = left, 3 = behind
func __tile_in_reach(tile: Vector2) -> int:
	var next_tile_info = STATE.get_tile_info(next_tile)
	if next_tile + orientation_vec.rotated(deg2rad(90)) == tile:
		match orientation_vec:
			Vector2.UP:
				if not next_tile_info.walls.right:
					return 1 # right of the player
			Vector2.RIGHT:
				if not next_tile_info.walls.bottom:
					return 1 # right of the player
			Vector2.DOWN:
				if not next_tile_info.walls.left:
					return 1 # right of the player
			Vector2.LEFT:
				if not next_tile_info.walls.top:
					return 1 # right of the player
		return -1 # not in reach
	elif next_tile + orientation_vec.rotated(deg2rad(-90)) == tile:
		match orientation_vec:
			Vector2.UP:
				if not next_tile_info.walls.left:
					return 2 # left of the player
			Vector2.RIGHT:
				if not next_tile_info.walls.top:
					return 2 # left of the player
			Vector2.DOWN:
				if not next_tile_info.walls.right:
					return 2 # left of the player
			Vector2.LEFT:
				if not next_tile_info.walls.bottom:
					return 2 # left of the player
		return -1 # not in reach
	elif next_tile - orientation_vec == tile:
		match orientation_vec:
			Vector2.UP:
				if not next_tile_info.walls.bottom:
					return 3 # behind the player
			Vector2.RIGHT:
				if not next_tile_info.walls.left:
					return 3 # behind the player
			Vector2.DOWN:
				if not next_tile_info.walls.top:
					return 3 # behind the player
			Vector2.LEFT:
				if not next_tile_info.walls.right:
					return 3 # behind the player
		return -1 # not in reach
	else:
		if not (tile - next_tile).normalized() in [orientation_vec, Vector2.ZERO]:
			return -1 # not in reach
		
		var check_tile = next_tile
		var hit_wall = false
		while not hit_wall:
			if tile == check_tile:
				return 0 # in front of the player
			
			var check_tile_info = STATE.get_tile_info(check_tile)
			match orientation_vec:
				Vector2.UP:
					if check_tile_info.walls.top:
						hit_wall = true
				Vector2.RIGHT:
					if check_tile_info.walls.right:
						hit_wall = true
				Vector2.DOWN:
					if check_tile_info.walls.bottom:
						hit_wall = true
				Vector2.LEFT:
					if check_tile_info.walls.left:
						hit_wall = true
			check_tile += orientation_vec
		return -1 # not in reach


func _on_adventurer_moving(new_tile: Vector2, adventurer: Adventurer):
	if class_id != ID.TEAMLEADER:
		return
	
	if adventurer_group.is_full(true) and adventurer.class_id != ID.TEAMLEADER:
		return
	
	var next_tile_info = STATE.get_tile_info(next_tile)
	if new_tile == next_tile:
		__add_to_group(adventurer)
	elif new_tile == next_tile + Vector2(0, -1) and not next_tile_info.walls.top:
		__add_to_group(adventurer)
	elif new_tile == next_tile + Vector2(1, 0) and not next_tile_info.walls.right:
		__add_to_group(adventurer)
	elif new_tile == next_tile + Vector2(0, 1) and not next_tile_info.walls.bottom:
		__add_to_group(adventurer)
	elif new_tile == next_tile + Vector2(-1, 0) and not next_tile_info.walls.left:
		__add_to_group(adventurer)


func __add_to_group(adventurer: Adventurer):
	if adventurer.class_id == ID.TEAMLEADER:
		adventurer_group.add_partner_group(adventurer.adventurer_group)
	else:
		adventurer_group.add_member(adventurer, true)


func set_adventurer_group(new_group: AdventurerGroup):
	adventurer_group = new_group
	adventurer_group.share_paths(astar_node)
	refresh_path(adventurer_group.get_leader_path())
	is_in_adventurer_group = true
	EVENTS.emit_signal('adventurer_joined_group', self)


func refresh_path(new_astar: AStar2D):
	for point_id in new_astar.get_points():
		var point_pos = new_astar.get_point_position(point_id)
		var own_point_id = __tile_in_astar(point_pos)
		if own_point_id == -1:
			var new_point_id = astar_node.get_available_point_id()
			astar_node.add_point(new_point_id, point_pos)
			for conn_id in new_astar.get_point_connections(point_id):
				var conn_pos = new_astar.get_point_position(conn_id)
				var own_conn_id = __tile_in_astar(conn_pos)
				if own_conn_id != -1:
					astar_node.connect_points(new_point_id, own_conn_id)
			if new_astar.is_point_disabled(point_id):
				astar_node.set_point_disabled(new_point_id)
		elif new_astar.is_point_disabled(point_id):
			astar_node.set_point_disabled(own_point_id)


func set_next_tile(tile: Vector2):
	old_tile = next_tile
	next_tile = tile
	if __tile_in_astar(tile) == -1:
		var new_point_id = astar_node.get_available_point_id()
		astar_node.add_point(new_point_id, tile)
		astar_node.connect_points(astar_node.get_closest_point(old_tile), new_point_id)

# called from the group
func set_tile_as_trap(trap_tile: Vector2, pos_tile: Vector2):
	var trap_tile_id = __tile_in_astar(trap_tile)
	var pos_tile_id = __tile_in_astar(pos_tile)
	if trap_tile_id == -1:
		var new_point_id = astar_node.get_available_point_id()
		astar_node.add_point(new_point_id, trap_tile)
		astar_node.connect_points(pos_tile_id, new_point_id)
	else:
		astar_node.set_point_disabled(trap_tile_id)


# called once a monster is on a neighboring tile
func get_attacked(monster):
	prints('new monster', monster)
	if not monster in attacking_monsters:
		attacking_monsters.append(monster)
	var monster_strength = 0
	var adventurer_strength = 0
	for attacking_monster in attacking_monsters:
		monster_strength += attacking_monster.strength
	if adventurer_group:
		adventurer_strength = adventurer_group.get_group_strength(true)
	else:
		adventurer_strength = strength
	if not $FightingTimer.is_stopped() and adventurer_strength > monster_strength:
		$FightingTimer.start()
		fighting_sprite.show()


func start_fight():
	monster_ready = true


# will search for new tiles
func __set_next_tile():
	if attacking_monsters.size() > 0:
		if monster_ready:
			$FightingTimer.start()
			fighting_sprite.show()
		return
	
	var tile_info = STATE.get_tile_info(next_tile)
	if tile_info.tile != null and tile_info.tile.type_id == ID.GOAL:
		EVENTS.emit_signal('adventurer_reached_goal')
		return
	
	old_tile = next_tile
	var directions = __find_new_tiles(tile_info, old_tile)
	var old_tile_id = astar_node.get_closest_point(old_tile)
	if directions.size() != 0:
		var removed_dir = []
		for direction in directions: # all directions need to be added
			var trap_id = __tile_is_trap(direction)
			var point_id = astar_node.get_available_point_id()
			astar_node.add_point(point_id, direction)
			astar_node.connect_points(old_tile_id, point_id)
			if trap_id != -1: # there is trap
				var direction_info = STATE.get_tile_info(direction)
				if adventurer_group: # only called by the leader
					if adventurer_group.spots_trap(trap_id):
						if adventurer_group.can_disarm_trap(trap_id) and $DisarmTimer.is_stopped():
							$DisarmTimer.start()
							disarming_sprite.show()
							next_tile = direction
							removed_dir.append(direction)
						else:
							if $DisarmTimer.is_stopped() and $SeeingTimer.is_stopped():
								$SeeingTimer.start()
								seeing_trap_sprite.show()
							astar_node.set_point_disabled(point_id)
							adventurer_group.share_trap(direction, old_tile)
							known_traps.append({ tile = direction, ref = direction_info.tile })
							removed_dir.append(direction)
				elif DATA.spots_trap([class_id], trap_id):
					if DATA.can_disarm_trap([class_id], trap_id) and $DisarmTimer.is_stopped():
						$DisarmTimer.start()
						disarming_sprite.show()
						next_tile = direction
						removed_dir.append(direction)
					else:
						if $DisarmTimer.is_stopped() and $SeeingTimer.is_stopped():
							$SeeingTimer.start()
							seeing_trap_sprite.show()
						astar_node.set_point_disabled(point_id)
						known_traps.append({ tile = direction, ref = direction_info.tile })
						removed_dir.append(direction)
			# TODO: add one way door
		for dir_vec in removed_dir:
			directions.erase(dir_vec)
		
		if directions.size() > 1 and $ThinkingTimer.is_stopped():
			$ThinkingTimer.start()
			thinking_sprite.show()
			possible_directions = directions
			return
		
		if next_tile == old_tile and directions.size() > 0:
			next_tile = directions.pop_at(UTIL.rng.randi() % directions.size())
		
		if next_tile != old_tile:
			orientation_vec = next_tile - old_tile
			
			for direction in directions: # only the ones where you don't go
				open_paths.append(direction)

			if class_id == ID.TEAMLEADER:
				adventurer_group.share_next_tile(next_tile)
			return
	
	var astar_points = astar_node.get_points().duplicate()
	astar_points.shuffle()
	for point_id in astar_points:
		var target_tile
		match class_id:
			ID.TEAMLEADER, ID.MAGICIAN, ID.GUNNER: # walks strategically through the labyrinth
				if open_paths.size() == 0:
					target_tile = astar_node.get_point_position(point_id)
					searching_new_path = true
				else:
					target_tile = open_paths.pop_back()
			ID.ROGUE, ID.WARRIOR: # walks random through the labyrinth
				if open_paths.size() == 0:
					target_tile = astar_node.get_point_position(point_id)
					searching_new_path = true
				else:
					target_tile = open_paths.pop_at(UTIL.rng.randi() % open_paths.size())
		target_path = Array(
			astar_node.get_point_path(
				old_tile_id,
				astar_node.get_closest_point(target_tile)
			)
		)
		if target_path.size() > 0:
			target_path.pop_front() # Throw away first one, because it's the same as old_tile
			break
	
	if target_path.size() == 0:
		is_stuck = true
	else:
		next_tile = target_path.pop_front()
		orientation_vec = next_tile - old_tile
	
	if class_id == ID.TEAMLEADER:
		adventurer_group.share_next_tile(next_tile)


# will follow path on already discovered tiles
func __follow_path():
	if attacking_monsters.size() > 0:
		if monster_ready:
			$FightingTimer.start()
			fighting_sprite.show()
		return
	
	var tile_info = STATE.get_tile_info(next_tile)
	old_tile = next_tile
	var directions = __find_new_tiles(tile_info, old_tile)
	var old_tile_id = astar_node.get_closest_point(old_tile)
	var removed_dir = []
	for direction in directions:
		var trap_id = __tile_is_trap(direction)
		var point_id = astar_node.get_available_point_id()
		astar_node.add_point(point_id, direction)
		astar_node.connect_points(old_tile_id, point_id)
		if trap_id != -1:
			var direction_info = STATE.get_tile_info(direction)
			if adventurer_group:
				if adventurer_group.spots_trap(trap_id):
					if (adventurer_group.can_disarm_trap(trap_id) and
						searching_new_path and
						$DisarmTimer.is_stopped()):
						$DisarmTimer.start()
						disarming_sprite.show()
						next_tile = direction
						removed_dir.append(direction)
						searching_new_path = false
					else:
						if $DisarmTimer.is_stopped() and $SeeingTimer.is_stopped():
							$SeeingTimer.start()
							seeing_trap_sprite.show()
						astar_node.set_point_disabled(point_id)
						adventurer_group.share_trap(direction, old_tile)
						known_traps.append({ tile = direction, ref = direction_info.tile })
						removed_dir.append(direction)
			elif DATA.spots_trap([class_id], trap_id):
				if (DATA.can_disarm_trap([class_id], trap_id) and
					searching_new_path and
					$DisarmTimer.is_stopped()):
					$DisarmTimer.start()
					disarming_sprite.show()
					next_tile = direction
					removed_dir.append(direction)
					searching_new_path = false
				else:
					if $DisarmTimer.is_stopped() and $SeeingTimer.is_stopped():
						$SeeingTimer.start()
						seeing_trap_sprite.show()
					astar_node.set_point_disabled(point_id)
					known_traps.append({ tile = direction, ref = direction_info.tile })
					removed_dir.append(direction)
	
	for dir_vec in removed_dir:
		directions.erase(dir_vec)
	
	if directions.size() > 0:
		if searching_new_path:
			searching_new_path = false
			target_path = []
			next_tile = directions.pop_at(UTIL.rng.randi() % directions.size())
			orientation_vec = next_tile - old_tile
			
			for direction in directions:
				open_paths.append(direction)
	
			if class_id == ID.TEAMLEADER:
				adventurer_group.share_next_tile(next_tile)
			return
		for direction in directions:
			open_paths.append(direction)
	
	if next_tile == old_tile:
		target_path = Array(
			astar_node.get_point_path(
				old_tile_id,
				astar_node.get_closest_point(target_path[-1])
			)
		)
		target_path.pop_front() # Throw first one away, because it's the old_tile
		if target_path.size() > 0:
			next_tile = target_path.pop_front()
			orientation_vec = next_tile - old_tile
	
	if class_id == ID.TEAMLEADER:
		adventurer_group.share_next_tile(next_tile)


# Find new tiles and reconnect / disconnect from existing because of new walls
func __find_new_tiles(tile_info: Dictionary, prev_tile: Vector2) -> Array:
	var directions = []
	var prev_tile_id = astar_node.get_closest_point(prev_tile)
	
	if not tile_info.walls.top and not (tile_info.tile != null and tile_info.tile.type_id == ID.ENTRANCE):
		var top_tile = prev_tile + Vector2(0, -1)
		var top_tile_info = STATE.get_tile_info(top_tile)
		var astar_index = __tile_in_astar(top_tile)
		if astar_index == -1:
			if __tile_in_known_traps(top_tile):
				var point_id = astar_node.get_available_point_id()
				astar_node.add_point(point_id, top_tile)
				astar_node.connect_points(prev_tile_id, point_id)
				astar_node.set_point_disabled(point_id)
			else:
				directions.append(top_tile)
		elif astar_index >= 0:
			if not astar_index in astar_node.get_point_connections(prev_tile_id):
				astar_node.connect_points(prev_tile_id, astar_index)
			if astar_node.is_point_disabled(astar_index):
				if top_tile_info.tile == null or not __is_same_trap(top_tile, top_tile_info.tile):
					astar_node.set_point_disabled(astar_index, false)
					__delete_known_trap(top_tile)
		
	elif not (tile_info.tile != null and tile_info.tile.type_id == ID.ENTRANCE):
		var top_tile = prev_tile + Vector2(0, -1)
		var astar_index = __tile_in_astar(top_tile)
		if astar_index in astar_node.get_point_connections(prev_tile_id):
			astar_node.disconnect_points(astar_index, prev_tile_id)
	
	if not tile_info.walls.right:
		var right_tile = prev_tile + Vector2(1, 0)
		var right_tile_info = STATE.get_tile_info(right_tile)
		var astar_index = __tile_in_astar(right_tile)
		if __tile_in_astar(right_tile) == -1:
			if __tile_in_known_traps(right_tile):
				var point_id = astar_node.get_available_point_id()
				astar_node.add_point(point_id, right_tile)
				astar_node.connect_points(prev_tile_id, point_id)
				astar_node.set_point_disabled(point_id)
			else:
				directions.append(right_tile)
		elif astar_index >= 0:
			if not astar_index in astar_node.get_point_connections(prev_tile_id):
				astar_node.connect_points(prev_tile_id, astar_index)
			if astar_node.is_point_disabled(astar_index):
				if right_tile_info.tile == null or not __is_same_trap(right_tile, right_tile_info.tile):
					astar_node.set_point_disabled(astar_index, false)
					__delete_known_trap(right_tile)
	else:
		var right_tile = prev_tile + Vector2(1, 0)
		var astar_index = __tile_in_astar(right_tile)
		if astar_index in astar_node.get_point_connections(prev_tile_id):
			astar_node.disconnect_points(astar_index, prev_tile_id)
	
	if not tile_info.walls.bottom:
		var bottom_tile = prev_tile + Vector2(0, 1)
		var bottom_tile_info = STATE.get_tile_info(bottom_tile)
		var astar_index = __tile_in_astar(bottom_tile)
		if __tile_in_astar(bottom_tile) == -1:
			if __tile_in_known_traps(bottom_tile):
				var point_id = astar_node.get_available_point_id()
				astar_node.add_point(point_id, bottom_tile)
				astar_node.connect_points(prev_tile_id, point_id)
				astar_node.set_point_disabled(point_id)
			else:
				directions.append(bottom_tile)
		elif astar_index >= 0:
			if not astar_index in astar_node.get_point_connections(prev_tile_id):
				astar_node.connect_points(prev_tile_id, astar_index)
			if astar_node.is_point_disabled(astar_index):
				if bottom_tile_info.tile == null or not __is_same_trap(bottom_tile, bottom_tile_info.tile):
					astar_node.set_point_disabled(astar_index, false)
					__delete_known_trap(bottom_tile)
	else:
		var bottom_tile = prev_tile + Vector2(0, 1)
		var astar_index = __tile_in_astar(bottom_tile)
		if astar_index in astar_node.get_point_connections(prev_tile_id):
			astar_node.disconnect_points(astar_index, prev_tile_id)
	
	if not tile_info.walls.left:
		var left_tile = prev_tile + Vector2(-1, 0)
		var left_tile_info = STATE.get_tile_info(left_tile)
		var astar_index = __tile_in_astar(left_tile)
		if __tile_in_astar(left_tile) == -1:
			if __tile_in_known_traps(left_tile):
				var point_id = astar_node.get_available_point_id()
				astar_node.add_point(point_id, left_tile)
				astar_node.connect_points(prev_tile_id, point_id)
				astar_node.set_point_disabled(point_id)
			else:
				directions.append(left_tile)
		elif astar_index >= 0:
			if not astar_index in astar_node.get_point_connections(prev_tile_id):
				astar_node.connect_points(prev_tile_id, astar_index)
			if astar_node.is_point_disabled(astar_index):
				if left_tile_info.tile == null or not __is_same_trap(left_tile, left_tile_info.tile):
					astar_node.set_point_disabled(astar_index, false)
					__delete_known_trap(left_tile)
	else:
		var left_tile = prev_tile + Vector2(-1, 0)
		var astar_index = __tile_in_astar(left_tile)
		if astar_index in astar_node.get_point_connections(prev_tile_id):
			astar_node.disconnect_points(astar_index, prev_tile_id)
	
	return directions


func __tile_in_astar(tile: Vector2) -> int:
	for index in astar_node.get_points():
		if tile == astar_node.get_point_position(index):
			return index
	return -1


func __tile_is_trap(tile: Vector2) -> int:
	var tile_info = STATE.get_tile_info(tile)
	if (tile_info.tile != null and not tile_info.tile.type_id in [ID.GOAL, ID.ENTRANCE] and
		tile_info.tile.is_active()):
		return tile_info.tile.type_id
	return -1


func __tile_in_known_traps(tile: Vector2) -> bool:
	for known_trap in known_traps:
		if known_trap.tile == tile:
			return true
	return false


func __delete_known_trap(tile: Vector2):
	var deletable_trap = []
	for known_trap in known_traps:
		if known_trap.tile == tile:
			deletable_trap.append(known_trap)
	for trap in deletable_trap:
		known_traps.erase(trap)


func __is_same_trap(tile: Vector2, trap) -> bool:
	for known_trap in known_traps:
		if known_trap.tile == tile:
			if known_trap.ref == trap:
				return true
			return false
	return false


func _on_thinking_timeout():
	thinking_sprite.hide()
	
	if attacking_monsters.size() > 0:
		if monster_ready:
			$FightingTimer.start()
			fighting_sprite.show()
		return
	
	next_tile = possible_directions.pop_at(UTIL.rng.randi() % possible_directions.size())
	orientation_vec = next_tile - old_tile

	for direction in possible_directions: # only the ones where you don't go
		open_paths.append(direction)

	if class_id == ID.TEAMLEADER:
		adventurer_group.share_next_tile(next_tile)


func _on_fighting_timeout():
	fighting_sprite.hide()
	monster_ready = false
	
	var monster_strength = 0
	var adventurer_strength = 0
	for attacking_monster in attacking_monsters:
		monster_strength += attacking_monster.strength
	if adventurer_group:
		adventurer_strength = adventurer_group.get_group_strength(true)
	else:
		adventurer_strength = strength
	
	if monster_strength > adventurer_strength:
		__death(1)
		var dmg_per_monster = adventurer_strength / attacking_monsters.size() # intentional int division
		if dmg_per_monster < 1:
			dmg_per_monster = 1
		for attacking_monster in attacking_monsters:
			attacking_monster.take_damage(dmg_per_monster)
	elif monster_strength < adventurer_strength:
		for attacking_monster in attacking_monsters:
			attacking_monster.take_damage(adventurer_strength)
		attacking_monsters = []
		# TODO: maybe level adventurers
	else:
		__death(1)
		for attacking_monster in attacking_monsters:
			attacking_monster.take_damage(adventurer_strength)


func _on_disarm_timeout():
	disarming_sprite.hide()
	
	var next_tile_info = STATE.get_tile_info(next_tile)
	next_tile_info.tile.disarmed()


func _on_seeing_timeout():
	seeing_trap_sprite.hide()


# reasons: 0 = Trap, 1 = Monster
func __death(reason: int):
	if reason == 0:
		if adventurer_group:
			var leading_group = adventurer_group.get_leading_group()
			var own_pg_index = leading_group.partner_groups.find(adventurer_group)
			if leading_group.partner_groups.size() > own_pg_index + 1:
				var new_leading_group = leading_group.partner_groups[own_pg_index + 1]
				new_leading_group.split_up()
				var next_tile_info = STATE.get_tile_info(next_tile)
				next_tile_info.tile.reduce_durability(adventurer_group.get_class_ids().size())
				adventurer_group.group_death(false, true)
		else:
			var next_tile_info = STATE.get_tile_info(next_tile)
			next_tile_info.tile.reduce_durability(1)
			EVENTS.emit_signal('adventurer_died', [class_id])
			queue_free()
	else:
		if adventurer_group:
			adventurer_group.group_death(true)
		else:
			EVENTS.emit_signal('adventurer_died', [class_id])
			queue_free()


func __refresh_sprite():
	match orientation_vec:
		Vector2.UP:
			if not up_sprite.is_visible():
				up_sprite.show()
			if right_sprite.is_visible():
				right_sprite.hide()
			if down_sprite.is_visible():
				down_sprite.hide()
			if left_sprite.is_visible():
				left_sprite.hide()
		Vector2.RIGHT:
			if up_sprite.is_visible():
				up_sprite.hide()
			if not right_sprite.is_visible():
				right_sprite.show()
			if down_sprite.is_visible():
				down_sprite.hide()
			if left_sprite.is_visible():
				left_sprite.hide()
		Vector2.DOWN:
			if up_sprite.is_visible():
				up_sprite.hide()
			if right_sprite.is_visible():
				right_sprite.hide()
			if not down_sprite.is_visible():
				down_sprite.show()
			if left_sprite.is_visible():
				left_sprite.hide()
		Vector2.LEFT:
			if up_sprite.is_visible():
				up_sprite.hide()
			if right_sprite.is_visible():
				right_sprite.hide()
			if down_sprite.is_visible():
				down_sprite.hide()
			if not left_sprite.is_visible():
				left_sprite.show()


func __refresh_icons():
	icon_offset += 0.05
	var new_offset = Vector2(0, sin(icon_offset) * 7)
	thinking_sprite.set_offset(new_offset)
	fighting_sprite.set_offset(new_offset)
	disarming_sprite.set_offset(new_offset)
	seeing_trap_sprite.set_offset(new_offset)


func _physics_process(delta):
	__refresh_sprite()
	__refresh_icons()
	
	if (not $ThinkingTimer.is_stopped() or
		not $DisarmTimer.is_stopped() or
		not $FightingTimer.is_stopped() or
		is_stuck):
		return
	
	var next_pos = STATE.get_world_position(next_tile)
	var current_pos = get_position() - pos_offset
	var dir_vec = (next_pos - current_pos).normalized()
	var new_pos
	if class_id == ID.ROGUE:
		new_pos = current_pos + (dir_vec * delta * ROGUE_MOVING_SPEED)
	else:
		new_pos = current_pos + (dir_vec * delta * MOVING_SPEED)
	var new_dir_vec = (next_pos - new_pos).normalized()
	if dir_vec + new_dir_vec == Vector2.ZERO:
		set_position(next_pos + pos_offset)
		if __tile_is_trap(next_tile) != -1:
			__death(0)
		elif target_path.size() > 0 and not is_in_adventurer_group:
			__follow_path()
			EVENTS.emit_signal('adventurer_moving', next_tile, self)
		elif not is_in_adventurer_group:
			__set_next_tile()
			EVENTS.emit_signal('adventurer_moving', next_tile, self)
	else:
		set_position(new_pos + pos_offset)
		if new_pos == next_pos:
			if __tile_is_trap(next_tile) != -1:
				__death(0)
			elif target_path.size() > 0 and not is_in_adventurer_group:
				__follow_path()
				EVENTS.emit_signal('adventurer_moving', next_tile, self)
			elif not is_in_adventurer_group:
				__set_next_tile()
				EVENTS.emit_signal('adventurer_moving', next_tile, self)
