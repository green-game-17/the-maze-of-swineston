extends Node2D

class_name Monster


const MOVING_SPEED = 200

signal died()

var type_id: int
var type_data: Dictionary
var old_tile: Vector2
var next_tile: Vector2
var orientation_vec: Vector2
var adventurer_ref: Dictionary
var strength: int # TODO: let adventurers have an effect on it
var astar_node: AStar2D
var waiting_for_adventurer = false
var attacking_adventurer = false


func _ready():
	$CheckTimer.connect('timeout', self, '_on_check_timeout')
	STATE.connect('maze_wall_moved', self, '_on_maze_wall_moved')
	STATE.connect('maze_extended', self, '_generate_astar')
	_generate_astar()
	
	set_position(STATE.get_world_position(old_tile))
	__check_for_closest_adv()
	var sprites = DATA.get_sprites(type_id)
	$UP.set_texture(sprites.up)
	$RIGHT.set_texture(sprites.right)
	$DOWN.set_texture(sprites.down)
	$LEFT.set_texture(sprites.left)
	__refresh_sprite()


func _generate_astar():
	astar_node = AStar2D.new()
	var tile_count = STATE.get_tile_count()
	if tile_count > 64:
		astar_node.reserve_space(tile_count)
	var maze = STATE.get_maze()
	for i in maze.size():
		for j in maze[i].size():
			var index = astar_node.get_available_point_id()
			var tile_info = STATE.get_tile_info(Vector2(j, i))
			astar_node.add_point(index, Vector2(j, i))
			if i > 0:
				if not tile_info.walls.top:
					var ind = (i - 1) * maze[0].size() + j
					astar_node.connect_points(index, ind)
			if j > 0:
				if not tile_info.walls.left:
					var ind = i * maze[0].size() + (j - 1)
					astar_node.connect_points(index, ind)


func set_data(data: Dictionary):
	type_id = data.type_id
	old_tile = data.tile
	next_tile = data.tile

	type_data = DATA.get_monster(data.type_id)
	strength = type_data.strength


func take_damage(damage: int):
	print('takes damage')
	strength -= damage
	attacking_adventurer = false
	if strength <= 0:
		emit_signal('died')
		queue_free()


# Fires when the monster doesnt follow an adventurer and checks for closest one
func _on_check_timeout():
	__check_for_closest_adv()


func _on_maze_wall_moved(from_tiles: Array, to_tiles: Array):
	var from_id_1 = astar_node.get_closest_point(from_tiles[0])
	var from_id_2 = astar_node.get_closest_point(from_tiles[1])
	var to_id_1 = astar_node.get_closest_point(to_tiles[0])
	var to_id_2 = astar_node.get_closest_point(to_tiles[1])
	astar_node.connect_points(from_id_1, from_id_2)
	astar_node.disconnect_points(to_id_1, to_id_2)
	
	if next_tile in to_tiles and old_tile in to_tiles:
		var current_pos = get_position()
		if ((current_pos - STATE.get_world_position(next_tile)).length_squared() >
			(current_pos - STATE.get_world_position(old_tile)).length_squared()):
			next_tile = old_tile
			orientation_vec -= orientation_vec * 2
			# TODO: abort fight with adventurer


func __check_for_closest_adv():
	old_tile = next_tile
	adventurer_ref = STATE.get_closest_adventurer(old_tile)
	if adventurer_ref.adv == null:
		waiting_for_adventurer = true
		$CheckTimer.start()
	else:
		waiting_for_adventurer = false
		var point_path = astar_node.get_point_path(
			astar_node.get_closest_point(old_tile),
			astar_node.get_closest_point(adventurer_ref.tile)
		)
		if point_path.size() > 2:
			next_tile = point_path[1]
			orientation_vec = next_tile - old_tile
			STATE.emit_signal('monster_moved', old_tile, next_tile, self)
		elif point_path.size() <= 2:
			if is_instance_valid(adventurer_ref.adv) and not is_queued_for_deletion():
				adventurer_ref.adv.get_attacked(self)
				attacking_adventurer = true
			# TODO: randomly wander around if no adventurer found


func __refresh_sprite():
	match orientation_vec:
		Vector2.UP:
			if not $UP.is_visible():
				$UP.show()
			if $RIGHT.is_visible():
				$RIGHT.hide()
			if $DOWN.is_visible():
				$DOWN.hide()
			if $LEFT.is_visible():
				$LEFT.hide()
		Vector2.RIGHT:
			if $UP.is_visible():
				$UP.hide()
			if not $RIGHT.is_visible():
				$RIGHT.show()
			if $DOWN.is_visible():
				$DOWN.hide()
			if $LEFT.is_visible():
				$LEFT.hide()
		Vector2.DOWN:
			if $UP.is_visible():
				$UP.hide()
			if $RIGHT.is_visible():
				$RIGHT.hide()
			if not $DOWN.is_visible():
				$DOWN.show()
			if $LEFT.is_visible():
				$LEFT.hide()
		Vector2.LEFT:
			if $UP.is_visible():
				$UP.hide()
			if $RIGHT.is_visible():
				$RIGHT.hide()
			if $DOWN.is_visible():
				$DOWN.hide()
			if not $LEFT.is_visible():
				$LEFT.show()


func _physics_process(delta):
	__refresh_sprite()
	if waiting_for_adventurer:
		return
	
	var next_pos = STATE.get_world_position(next_tile)
	var current_pos = get_position()
	var dir_vec = (next_pos - current_pos).normalized()
	var new_pos = current_pos + (dir_vec * delta * MOVING_SPEED)
	var new_dir_vec = (next_pos - new_pos).normalized()
	if dir_vec + new_dir_vec == Vector2.ZERO:
		set_position(next_pos)
		if not attacking_adventurer:
			__check_for_closest_adv()
		elif is_instance_valid(adventurer_ref.adv):
			adventurer_ref.adv.start_fight()
	else:
		set_position(new_pos)
		if new_pos == next_pos:
			if not attacking_adventurer:
				__check_for_closest_adv()
			elif is_instance_valid(adventurer_ref.adv):
				adventurer_ref.adv.start_fight()
