extends Node2D


onready var maze_map: TileMap = $MazeMap
onready var maze_tile_set: TileSet = maze_map.tile_set
onready var maze_tile_dict = {
	corner_bl = maze_tile_set.find_tile_by_name('corner_bl'),
	corner_br = maze_tile_set.find_tile_by_name('corner_br'),
	corner_tl = maze_tile_set.find_tile_by_name('corner_tl'),
	corner_tr = maze_tile_set.find_tile_by_name('corner_tr'),
	edge_b = maze_tile_set.find_tile_by_name('edge_b'),
	edge_b_join = maze_tile_set.find_tile_by_name('edge_b_join'),
	edge_b_l = maze_tile_set.find_tile_by_name('edge_b_l'),
	edge_b_l_join = maze_tile_set.find_tile_by_name('edge_b_l_join'),
	edge_b_r = maze_tile_set.find_tile_by_name('edge_b_r'),
	edge_b_r_join = maze_tile_set.find_tile_by_name('edge_b_r_join'),
	edge_l = maze_tile_set.find_tile_by_name('edge_l'),
	edge_l_join = maze_tile_set.find_tile_by_name('edge_l_join'),
	edge_r = maze_tile_set.find_tile_by_name('edge_r'),
	edge_r_join = maze_tile_set.find_tile_by_name('edge_r_join'),
	edge_t = maze_tile_set.find_tile_by_name('edge_t'),
	edge_t_join = maze_tile_set.find_tile_by_name('edge_t_join'),
	edge_t_l = maze_tile_set.find_tile_by_name('edge_t_l'),
	edge_t_l_join = maze_tile_set.find_tile_by_name('edge_t_l_join'),
	edge_t_r = maze_tile_set.find_tile_by_name('edge_t_r'),
	edge_t_r_join = maze_tile_set.find_tile_by_name('edge_t_r_join'),
	wall_b = maze_tile_set.find_tile_by_name('wall_b'),
	wall_bl = maze_tile_set.find_tile_by_name('wall_bl'),
	wall_l = maze_tile_set.find_tile_by_name('wall_l'),
	wall_r = maze_tile_set.find_tile_by_name('wall_r'),
	wall_rb = maze_tile_set.find_tile_by_name('wall_rb'),
	wall_rbl = maze_tile_set.find_tile_by_name('wall_rbl'),
	wall_rl = maze_tile_set.find_tile_by_name('wall_rl'),
	wall_t = maze_tile_set.find_tile_by_name('wall_t'),
	wall_tb = maze_tile_set.find_tile_by_name('wall_tb'),
	wall_tbl = maze_tile_set.find_tile_by_name('wall_tbl'),
	wall_tl = maze_tile_set.find_tile_by_name('wall_tl'),
	wall_tr = maze_tile_set.find_tile_by_name('wall_tr'),
	wall_trb = maze_tile_set.find_tile_by_name('wall_trb'),
	wall_trl = maze_tile_set.find_tile_by_name('wall_trl'),
	wall_x = maze_tile_set.find_tile_by_name('wall_x'),
	ground = [
		maze_tile_set.find_tile_by_name('ground_0'),
		maze_tile_set.find_tile_by_name('ground_1'),
		maze_tile_set.find_tile_by_name('ground_2'),
		maze_tile_set.find_tile_by_name('ground_3'),
		maze_tile_set.find_tile_by_name('ground_4'),
		maze_tile_set.find_tile_by_name('ground_5'),
		maze_tile_set.find_tile_by_name('ground_6'),
		maze_tile_set.find_tile_by_name('ground_7'),
		maze_tile_set.find_tile_by_name('ground_8'),
		maze_tile_set.find_tile_by_name('ground_9'),
	],
	attack_turtle = maze_tile_set.find_tile_by_name('attack_turtle')
}
onready var ground_overlay_h = $GroundOverlayH
onready var wall_overlay_h = $WallOverlayH
onready var ground_overlay_v = $GroundOverlayV
onready var wall_overlay_v = $WallOverlayV
onready var entity_overlay = $EntityOverlay

onready var notification_scene = load('res://components/notification/notification.tscn')

var astar_node: AStar2D

var editing_tile_x = null
var editing_tile_y = null
var editing_tile_wall_dir = null
var editing_tile_wall_offset = null
var editing_tile_wall_top_possible = null
var editing_tile_wall_left_possible = null
var editing_tile_wall_bottom_possible = null
var editing_tile_wall_right_possible = null
var editing_tile_entity_id = null
var editing_tile_entity_data = null
var editing_tile_delete_active = false
var editing_tile_delete_last_hover = null


func _ready():
	__rebuild_maze()

	EVENTS.connect('non_ui_mouse_click', self, '_on_non_ui_mouse_click')

	EVENTS.connect('start_entity_placing', self, '_on_start_entity_placing')
	EVENTS.connect('abort_entity_placing', self, '_on_end_entity_editing', [null, null])
	EVENTS.connect('abort_entity_placing', self, '_on_end_wall_editing')
	
	EVENTS.connect('start_tile_deleting', self, '_on_start_tile_deleting')
	EVENTS.connect('start_tile_deleting', self, '_on_end_wall_editing')
	EVENTS.connect('abort_tile_deleting', self, '_on_end_tile_deleting')

	STATE.connect('maze_extended', self, '__rebuild_maze')
	STATE.connect('maze_trap_placed', self, '_on_end_entity_editing', [null])
	STATE.connect('maze_monster_placed', self, '_on_end_entity_editing')


func _process(_delta):
	pass


func _input(event):
	var cell_pos = maze_map.world_to_map(get_global_mouse_position())
	var tile_pos_x = int(cell_pos.x / DATA.GRID_SIZE)
	var tile_pos_y = int(cell_pos.y / DATA.GRID_SIZE)

	if Input.is_action_just_pressed('build_abort'):
		if editing_tile_wall_dir:
			_on_end_wall_editing()
			return

		if editing_tile_entity_id:
			EVENTS.emit_signal('abort_entity_placing')
			return

		if editing_tile_delete_active:
			EVENTS.emit_signal('abort_tile_deleting')

	if event is InputEventMouseMotion:
		var maze = STATE.get_maze()

		if tile_pos_x < 0 or tile_pos_x >= maze[0].size() or tile_pos_y < 0 or tile_pos_y >= maze.size():
			return

		if editing_tile_wall_dir == 'bottom':
			if editing_tile_wall_left_possible and tile_pos_x < editing_tile_x:
				editing_tile_wall_offset = -1
			elif editing_tile_wall_right_possible and tile_pos_x > editing_tile_x:
				editing_tile_wall_offset = +1
			wall_overlay_h.position = Vector2(
				(editing_tile_x+editing_tile_wall_offset)*DATA.GRID_PX_SIZE, editing_tile_y*DATA.GRID_PX_SIZE+DATA.TILE_PX_SIZE
			)
		elif editing_tile_wall_dir == 'right':
			if editing_tile_wall_top_possible and tile_pos_y < editing_tile_y:
				editing_tile_wall_offset = -1
			elif editing_tile_wall_bottom_possible and tile_pos_y > editing_tile_y:
				editing_tile_wall_offset = +1
			wall_overlay_v.position = Vector2(
				editing_tile_x*DATA.GRID_PX_SIZE+DATA.TILE_PX_SIZE, (editing_tile_y+editing_tile_wall_offset)*DATA.GRID_PX_SIZE
			)

		if editing_tile_entity_id:
			if maze[tile_pos_y][tile_pos_x].tile == null:
				editing_tile_x = tile_pos_x
				editing_tile_y = tile_pos_y
				entity_overlay.position = Vector2(
					editing_tile_x*DATA.GRID_PX_SIZE, editing_tile_y*DATA.GRID_PX_SIZE
				)

		if editing_tile_delete_active:
			if (
				maze[tile_pos_y][tile_pos_x].tile != null 
				and typeof(maze[tile_pos_y][tile_pos_x].tile) != TYPE_DICTIONARY
			):
				editing_tile_x = tile_pos_x
				editing_tile_y = tile_pos_y
				if editing_tile_delete_last_hover != null:
					editing_tile_delete_last_hover.modulate = Color.white
				editing_tile_delete_last_hover = maze[tile_pos_y][tile_pos_x].tile
				editing_tile_delete_last_hover.modulate.g = 0.5
				editing_tile_delete_last_hover.modulate.b = 0.5
				editing_tile_delete_last_hover.modulate.a = 0.8


func _on_non_ui_mouse_click():
	var cell_pos = maze_map.world_to_map(get_global_mouse_position())
	var tile_pos_x = int(cell_pos.x / DATA.GRID_SIZE)
	var tile_pos_y = int(cell_pos.y / DATA.GRID_SIZE)

	if editing_tile_wall_dir:
		_on_accept_wall_editing()
		return

	if editing_tile_entity_id:
		_on_accept_entity_editing()
		return

	if editing_tile_delete_active:
		_on_accept_tile_deleting()
		return

	var is_right_wall = false
	var is_bottom_wall = false
	if cell_pos.x == tile_pos_x * DATA.GRID_SIZE + DATA.TILE_SIZE:
		is_right_wall = true
	if cell_pos.y == tile_pos_y * DATA.GRID_SIZE + DATA.TILE_SIZE:
		is_bottom_wall = true
	
	var wall_dir = null
	if is_right_wall && not is_bottom_wall:
		wall_dir = 'right'
	elif not is_right_wall && is_bottom_wall:
		wall_dir = 'bottom'

	if wall_dir != null:
		_on_start_wall_editing(tile_pos_x, tile_pos_y, wall_dir)
	else:
		_on_tile_clicked(tile_pos_x, tile_pos_y)


func _on_start_wall_editing(x: int, y: int, dir: String):
	var maze = STATE.get_maze()

	if (
		x < 0 or x >= maze[0].size()
	) or (
		y < 0 or y >= maze.size()
	) or (
		not maze[y][x].walls[dir]
	):
		return

	if STATE.get_life_force_count() < DATA.LIFE_FORCE_PER_WALL_MOVE:
		var notification_pos = Vector2(x*DATA.GRID_PX_SIZE, y*DATA.GRID_PX_SIZE)
		if dir == 'bottom':
			notification_pos.y += DATA.TILE_PX_SIZE
		var new_notification = notification_scene.instance()
		new_notification.set_data('Not enough life force to move wall', false)
		new_notification.set_position(notification_pos)
		add_child(new_notification)
		return

	if dir == 'bottom':
		editing_tile_wall_right_possible = x<maze[0].size()-1 and not maze[y][x+1].walls.bottom
		editing_tile_wall_left_possible = x>0 and not maze[y][x-1].walls.bottom

		if not editing_tile_wall_left_possible and not editing_tile_wall_right_possible:
			return
		
		ground_overlay_h.show()
		ground_overlay_h.position = Vector2(x*DATA.GRID_PX_SIZE, y*DATA.GRID_PX_SIZE+DATA.TILE_PX_SIZE)

		wall_overlay_h.show()
		if editing_tile_wall_right_possible:
			wall_overlay_h.position = Vector2((x+1)*DATA.GRID_PX_SIZE, y*DATA.GRID_PX_SIZE+DATA.TILE_PX_SIZE)
			editing_tile_wall_offset = +1
		elif editing_tile_wall_left_possible:
			wall_overlay_h.position = Vector2((x-1)*DATA.GRID_PX_SIZE, y*DATA.GRID_PX_SIZE+DATA.TILE_PX_SIZE)
			editing_tile_wall_offset = -1

	elif dir == 'right':
		editing_tile_wall_bottom_possible = y<maze.size()-1 and not maze[y+1][x].walls.right
		editing_tile_wall_top_possible = y>0 and not maze[y-1][x].walls.right

		if not editing_tile_wall_top_possible and not editing_tile_wall_bottom_possible:
			return
		
		ground_overlay_v.show()
		ground_overlay_v.position = Vector2(x*DATA.GRID_PX_SIZE+DATA.TILE_PX_SIZE, y*DATA.GRID_PX_SIZE)

		wall_overlay_v.show()
		if editing_tile_wall_bottom_possible:
			wall_overlay_v.position = Vector2(x*DATA.GRID_PX_SIZE+DATA.TILE_PX_SIZE, (y+1)*DATA.GRID_PX_SIZE)
			editing_tile_wall_offset = +1
		elif editing_tile_wall_top_possible:
			wall_overlay_v.position = Vector2(x*DATA.GRID_PX_SIZE+DATA.TILE_PX_SIZE, (y-1)*DATA.GRID_PX_SIZE)
			editing_tile_wall_offset = -1
			
	editing_tile_wall_dir = dir
	editing_tile_x = x
	editing_tile_y = y


func _on_accept_wall_editing():
	var dir = editing_tile_wall_dir
	var x = editing_tile_x
	var y = editing_tile_y


	if dir == null or x == null or y == null:
		_on_end_wall_editing() # hide overlays
		return

	var original_tile_pos = Vector2(x, y)
	var inverse_original_tile_pos = null
	var target_tile_pos = null
	var inverse_target_tile_pos = null
	var inverse_dir = null
	var notification_pos = null

	if dir == 'bottom':
		inverse_original_tile_pos = Vector2(x, y+1)
		target_tile_pos = Vector2(x+editing_tile_wall_offset, y)
		inverse_target_tile_pos = Vector2(x+editing_tile_wall_offset, y+1)
		inverse_dir = 'top'
		notification_pos = wall_overlay_h.position
	elif dir == 'right':
		inverse_original_tile_pos = Vector2(x+1, y)
		target_tile_pos = Vector2(x, y+editing_tile_wall_offset)
		inverse_target_tile_pos = Vector2(x+1, y+editing_tile_wall_offset)
		inverse_dir = 'left'
		notification_pos = wall_overlay_v.position

	if target_tile_pos == null:
		return

	var orig_id = astar_node.get_closest_point(original_tile_pos)
	var inv_orig_id = astar_node.get_closest_point(inverse_original_tile_pos)
	var targ_id = astar_node.get_closest_point(target_tile_pos)
	var inv_targ_id = astar_node.get_closest_point(inverse_target_tile_pos)
	var start_id = astar_node.get_closest_point(STATE.get_start_tile())
	var goal_id = astar_node.get_closest_point(STATE.get_goal_tile())
	astar_node.connect_points(orig_id, inv_orig_id)
	astar_node.disconnect_points(targ_id, inv_targ_id)
	var path_to_goal = astar_node.get_point_path(start_id, goal_id)
	if path_to_goal.size() <= 1:
		astar_node.disconnect_points(orig_id, inv_orig_id)
		astar_node.connect_points(targ_id, inv_targ_id)

		var new_notification = notification_scene.instance()
		new_notification.set_data('Would fully block the goal', false)
		new_notification.set_position(notification_pos)
		add_child(new_notification)

		return

	_on_end_wall_editing() # hide overlays

	var new_notification = notification_scene.instance()
	new_notification.set_data('- %d' % DATA.LIFE_FORCE_PER_WALL_MOVE)
	new_notification.set_position(notification_pos)
	add_child(new_notification)

	STATE.move_wall_in_maze(
		original_tile_pos, inverse_original_tile_pos, target_tile_pos, inverse_target_tile_pos, dir, inverse_dir
	)
	STATE.change_life_force_count(-1 * DATA.LIFE_FORCE_PER_WALL_MOVE)

	__rebuild_maze()


func _on_end_wall_editing():
	editing_tile_wall_dir = null
	editing_tile_x = null
	editing_tile_y = null
	ground_overlay_h.hide()
	wall_overlay_h.hide()
	ground_overlay_v.hide()
	wall_overlay_v.hide()


func _on_start_entity_placing(id, data):
	editing_tile_entity_id = id
	editing_tile_entity_data = data
	if data.sprites.has('down'):
		entity_overlay.set_texture(data.sprites.down)
	elif data.sprites.has('normal'):
		entity_overlay.set_texture(data.sprites.normal)
	entity_overlay.position = Vector2(-1000, -1000)
	entity_overlay.show()


func _on_accept_entity_editing():
	if editing_tile_entity_id != null and editing_tile_x != null and editing_tile_y != null:
		var new_notification = notification_scene.instance()
		new_notification.set_data('- %d' % editing_tile_entity_data.cost)
		new_notification.set_position(entity_overlay.position)
		add_child(new_notification)

		STATE.place_entity_in_maze(editing_tile_entity_id, editing_tile_x, editing_tile_y)


func _on_end_entity_editing(_a1, _a2):
	editing_tile_entity_id = null
	editing_tile_entity_data = null
	editing_tile_x = null
	editing_tile_y = null
	entity_overlay.hide()


func _on_start_tile_deleting():
	editing_tile_delete_active = true


func _on_accept_tile_deleting():
	if editing_tile_delete_last_hover == null or editing_tile_x == null or editing_tile_y == null:
		return

	var x = editing_tile_x
	var y = editing_tile_y

	var new_notification = notification_scene.instance()
	var restored_cost = (
		DATA.get_life_force(editing_tile_delete_last_hover.type_id)
		* DATA.DESTRUCT_LIFE_FORCE_RESTORE_SHARE
	)
	new_notification.set_data('+ %d' % restored_cost, true, Color(0.15, 0.9, 0.5))
	new_notification.set_position(Vector2(x, y) * DATA.GRID_PX_SIZE)
	add_child(new_notification)

	editing_tile_delete_last_hover = null
	editing_tile_x = null
	editing_tile_y = null
	
	STATE.remove_tile_from_maze(x, y)


func _on_end_tile_deleting():
	editing_tile_delete_active = false
	editing_tile_x = null
	editing_tile_y = null
	if editing_tile_delete_last_hover:
		editing_tile_delete_last_hover.modulate = Color.white
		editing_tile_delete_last_hover = null

	
func _on_tile_clicked(x: int, y: int):
	pass


func __rebuild_maze():
	maze_map.clear()

	var maze = STATE.get_maze()
	var maze_w = len(maze[0])
	var maze_h = len(maze)
	var maze_w_cells = maze_w * (DATA.GRID_SIZE) - 1
	var maze_h_cells = maze_h * (DATA.GRID_SIZE) - 1

	for iy in range(len(maze)):
		var maze_row = maze[iy]

		# left edge:
		for i in range(DATA.TILE_SIZE):
			__set_maze_cell(-1, iy*DATA.GRID_SIZE+i, 'edge_l')

		# bottom left edge corner:
		var name_bl = 'edge_l'
		if maze_row[0].walls.bottom:
			name_bl += '_join'
		__set_maze_cell(-1, iy*DATA.GRID_SIZE+DATA.TILE_SIZE, name_bl)
		
		# tiles:
		for ix in range(maze_w):
			var tile = maze_row[ix]

			# top edge:
			if iy == 0:
				var name = 'edge_t' if tile.walls.top else 'ground'
				for i in range(DATA.TILE_SIZE):
					__set_maze_cell(ix*DATA.GRID_SIZE+i, -1, name)

			# inner tile ground:
			for tiy in range(iy*(DATA.GRID_SIZE), (iy+1)*(DATA.GRID_SIZE)):
				for tix in range(ix*(DATA.GRID_SIZE), (ix+1)*(DATA.GRID_SIZE)):
					__set_maze_cell(tix, tiy, 'ground')

			# tile walls:
			if tile.walls.bottom:
				var name = 'wall_tb' if iy != maze_h - 1 else 'edge_b'
				for i in range(DATA.TILE_SIZE):
					__set_maze_cell(ix*DATA.GRID_SIZE+i, (iy+1)*DATA.GRID_SIZE-1, name)
			if tile.walls.right:
				var name = 'wall_rl' if ix != maze_w - 1 else 'edge_r'
				for i in range(DATA.TILE_SIZE):
					__set_maze_cell((ix+1)*DATA.GRID_SIZE-1, iy*DATA.GRID_SIZE+i, name)

			# tile top right corner:
			if iy == 0:
				var name_tr = 'edge_t'
				if ix < maze_w - 1 && not maze_row[ix+1].walls.top:
					name_tr += '_r'
				elif not tile.walls.top:
					name_tr += '_l'
				if tile.walls.right:
					name_tr += '_join'
				__set_maze_cell((ix+1)*DATA.GRID_SIZE-1, -1, name_tr)
			
			# tile bottom right corner:
			var name_br = null
			if iy == maze_h - 1:
				name_br = 'edge_b'
				if ix < maze_w - 1 && not maze_row[ix+1].walls.bottom:
					name_br += '_r'
				elif not tile.walls.bottom:
					name_br += '_l'
				if tile.walls.right:
					name_br += '_join'
			elif ix == maze_w - 1:
				name_br = 'edge_r'
				if tile.walls.bottom:
					name_br += '_join'
			else:
				name_br = __get_corner_tile_name(
					tile.walls.right, maze[iy][ix+1].walls.bottom, maze[iy+1][ix].walls.right, tile.walls.bottom
				)
			if name_br != null:
				__set_maze_cell((ix+1)*DATA.GRID_SIZE-1, iy*DATA.GRID_SIZE+DATA.TILE_SIZE, name_br)

	# outer corners:
	__set_maze_cell(-1, -1, 'corner_tl')
	__set_maze_cell(maze_w_cells, -1, 'corner_tr')
	__set_maze_cell(-1, maze_h_cells, 'corner_bl')
	__set_maze_cell(maze_w_cells, maze_h_cells, 'corner_br')

	# attack turtle:
	var goal_pos = STATE.get_goal_tile()
	__set_maze_cell(goal_pos.x * DATA.GRID_SIZE + 1, (goal_pos.y + 1) * DATA.GRID_SIZE + 1, 'attack_turtle')

	# A*
	__generate_astar()


func __get_corner_tile_name(t, r, b, l):
	var suffix = ''
	if not t:
		suffix += 't'
	if not r:
		suffix += 'r'
	if not b:
		suffix += 'b'
	if not l:
		suffix += 'l'
	
	if suffix == 'trbl':
		return 'ground'

	if suffix == '':
		suffix = 'x'

	return 'wall_' + suffix


func __update_maze_tile(x: int, y: int, data):
	pass


func __set_maze_cell(x: int, y: int, name: String):
	var tile = maze_tile_dict[name]
	if name == 'ground':
		tile = tile[UTIL.rng.randi_range(0, tile.size()-1)]
		maze_map.set_cell(x, y, tile, UTIL.rng.randf() > 0.5, UTIL.rng.randf() > 0.5)
	else:
		maze_map.set_cell(x, y, tile)


func __generate_astar():
	astar_node = AStar2D.new()
	var tile_count = STATE.get_tile_count()
	if tile_count > 64:
		astar_node.reserve_space(tile_count)
	var maze = STATE.get_maze()
	for i in maze.size():
		for j in maze[i].size():
			var index = astar_node.get_available_point_id()
			var tile_info = STATE.get_tile_info(Vector2(j, i))
			astar_node.add_point(index, Vector2(j, i))
			if i > 0:
				if not tile_info.walls.top:
					var ind = (i - 1) * maze[0].size() + j
					astar_node.connect_points(index, ind)
			if j > 0:
				if not tile_info.walls.left:
					var ind = i * maze[0].size() + (j - 1)
					astar_node.connect_points(index, ind)
