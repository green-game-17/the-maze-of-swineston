extends Node


# Mutable, shared state data:
const TILE_SCALING = 512
const OFFSET = Vector2(192, 192)
const INITIAL_LIFE_FORCE_COUNT = 150
const MAZE_WIDTH = 16
const MAZE_HEIGHT_PER_CHUNK = 5

const MIN_SOULS_FOR_HARDER_CLASSES = 300


signal maze_extended()
signal maze_wall_moved(originals, targets) # ( [Vector2, Vector2], [Vector2, Vector2] )
signal maze_trap_placed(tiles) # [Vector2]
signal maze_monster_placed(tile, id) # Vector2, int
signal monster_moved(old_tile, new_tile) # Vector2, Vector2
signal playtime_advanced(time) # int
signal souls_count_changed(count) # int
signal life_force_count_changed(count) # int
signal no_adventurer_alive() # int


onready var trap_scene = load('res://components/trap/trap.tscn')
onready var monster_scene = load('res://components/monster/monster.tscn')

onready var traps_node = get_node_or_null('/root/World/World/Traps')
onready var monsters_node = get_node_or_null('/root/World/World/Monsters')


var start_tile = Vector2(0, 0)
var goal_tile = Vector2(0, 0)
var maze = [[]]
var adventurers = []
var adventurers_alive = 0
var playtime = 0
var souls = 0
var life_force = 0


func _ready():
	reset_maze()

	reset()

	__advance_timer()

	EVENTS.connect('adventurer_moving', self, '_on_adventurer_moving')
	EVENTS.connect('adventurer_joined_group', self, '_on_adventurer_joined_group')
	EVENTS.connect('adventurer_died', self, '_on_adventurer_died')


func get_start_tile() -> Vector2:
	return start_tile


func get_goal_tile() -> Vector2:
	return goal_tile


func get_tile_count() -> int:
	return maze.size() * maze[0].size()


func get_world_position(pos: Vector2) -> Vector2:
	return (pos * TILE_SCALING) + OFFSET


func get_tile_info(tile: Vector2) -> Dictionary:
	return maze[tile.y][tile.x]


func get_maze() -> Array:
	return maze.duplicate(true)


func move_wall_in_maze(
	original_tile_pos: Vector2, inverse_original_tile_pos: Vector2, 
	target_tile_pos: Vector2, inverse_target_tile_pos: Vector2,
	dir: String, inverse_dir: String
):
	maze[original_tile_pos.y][original_tile_pos.x].walls[dir] = false
	maze[target_tile_pos.y][target_tile_pos.x].walls[dir] = true
	maze[inverse_original_tile_pos.y][inverse_original_tile_pos.x].walls[inverse_dir] = false
	maze[inverse_target_tile_pos.y][inverse_target_tile_pos.x].walls[inverse_dir] = true

	self.emit_signal('maze_wall_moved', 
		[original_tile_pos, inverse_original_tile_pos], [target_tile_pos, inverse_target_tile_pos]
	)


func place_entity_in_maze(
	id: int,
	x: int, y: int
):
	if x < 0 or x >= maze[0].size() or y < 0 or y >= maze.size():
		return

	var v = Vector2(x, y)

	if DATA.TRAPS.has(id):
		var new_trap = trap_scene.instance()
		new_trap.set_data({ type_id = id, tiles = [v] })
		traps_node.add_child(new_trap)
		maze[y][x].tile = new_trap

		change_life_force_count(-1 * DATA.TRAPS[id].cost)

		emit_signal('maze_trap_placed', [v])

	elif DATA.MONSTERS.has(id):
		var new_monster = monster_scene.instance()
		new_monster.set_data({ type_id = id, tile = v })
		monsters_node.add_child(new_monster)
		maze[y][x].monster = new_monster

		change_life_force_count(-1 * DATA.MONSTERS[id].cost)

		emit_signal('maze_monster_placed', v, id)


func remove_tile_from_maze(x: int, y: int):
	if maze[y][x].tile == null:
		return

	var recovered_cost = DATA.get_life_force(maze[y][x].tile.type_id) * DATA.DESTRUCT_LIFE_FORCE_RESTORE_SHARE
	change_life_force_count(recovered_cost)

	maze[y][x].tile.queue_free()
	maze[y][x].tile = null


func reset():
	adventurers = []
	playtime = 0
	souls = 1
	life_force = INITIAL_LIFE_FORCE_COUNT

	traps_node = get_node_or_null('/root/World/World/Traps')
	monsters_node = get_node_or_null('/root/World/World/Monsters')


func regenerate_debug_maze():
	maze = [
		[
			{ tile = null,                      monster = null, walls = {top = true , right = false, bottom = false, left = true } },
			{ tile = null,                      monster = null, walls = {top = true , right = false, bottom = true , left = false} },
			{ tile = { type_id = ID.ENTRANCE }, monster = null, walls = {top = false, right = true,  bottom = true , left = false} },
			{ tile = null,                      monster = null, walls = {top = true , right = false, bottom = false, left = true } },
			{ tile = null,                      monster = null, walls = {top = true , right = true , bottom = true , left = false} },
		],
		[
			{ tile = null,                      monster = null, walls = {top = false, right = false, bottom = true , left = true } },
			{ tile = null,                      monster = null, walls = {top = true , right = false, bottom = true , left = false} },
			{ tile = null,                      monster = null, walls = {top = true , right = false, bottom = false, left = false} },
			{ tile = null,                      monster = null, walls = {top = false, right = false, bottom = true , left = false} },
			{ tile = null,                      monster = null, walls = {top = true , right = true , bottom = false, left = false} },
		],
		[
			{ tile = null,                      monster = null, walls = {top = true , right = false, bottom = false, left = true } },
			{ tile = null,                      monster = null, walls = {top = true , right = true , bottom = false, left = false} },
			{ tile = null,                      monster = null, walls = {top = false, right = true , bottom = false, left = true } },
			{ tile = null,                      monster = null, walls = {top = true , right = true , bottom = true , left = true } },
			{ tile = null,                      monster = null, walls = {top = false, right = true , bottom = false, left = true } },
		],
		[
			{ tile = null,                      monster = null, walls = {top = false, right = false, bottom = false, left = true } },
			{ tile = null,                      monster = null, walls = {top = false, right = true , bottom = false, left = false} },
			{ tile = null,                      monster = null, walls = {top = false, right = false, bottom = false, left = true } },
			{ tile = null,                      monster = null, walls = {top = true , right = false, bottom = true , left = false} },
			{ tile = null,                      monster = null, walls = {top = false, right = true , bottom = true , left = false} },
		],
		[
			{ tile = null,                      monster = null, walls = {top = false, right = false, bottom = true , left = true } },
			{ tile = null,                      monster = null, walls = {top = false, right = true , bottom = true , left = false} },
			{ tile = { type_id = ID.GOAL },     monster = null, walls = {top = false, right = true , bottom = false, left = true } },
			{ tile = null,                      monster = null, walls = {top = true , right = false, bottom = true , left = true } },
			{ tile = null,                      monster = null, walls = {top = true , right = true , bottom = true , left = false} },
		]
	]
	start_tile = Vector2(2, 0)
	goal_tile = Vector2(2, 4)


func reset_maze():
	# init search space:
	var new_maze = UTIL.filled_matrix(MAZE_WIDTH, MAZE_HEIGHT_PER_CHUNK, {
		tile = null,
		monster = null,
		walls = { top = true, right = true, bottom = true, left = true }
	})

	__build_maze(new_maze, [])

	# randomly place entry:
	var start_x = UTIL.rng.randi_range(1, MAZE_WIDTH-2)
	maze[0][start_x].tile = { type_id = ID.ENTRANCE }
	maze[0][start_x].walls.top = false
	start_tile = Vector2(start_x, 0)

	emit_signal('maze_extended')


func extend_maze():
	var old_maze = maze.duplicate(true)

	# remove old goal:
	old_maze[goal_tile.y][goal_tile.x].tile = null

	# tear up bottom walls to connect:
	var possible_wall_x = range(MAZE_WIDTH)
	possible_wall_x.shuffle()
	var teared_down_walls_x = possible_wall_x.slice(0, MAZE_WIDTH/3*2)

	var old_bottom_row = old_maze[old_maze.size()-1]
	for x in teared_down_walls_x:
		old_bottom_row[x].walls.bottom = false

	# init search space:
	var new_maze = UTIL.filled_matrix(MAZE_WIDTH, MAZE_HEIGHT_PER_CHUNK, {
		tile = null,
		monster = null,
		walls = { top = true, right = true, bottom = true, left = true }
	})
	var new_top_row = new_maze[0]
	for x in teared_down_walls_x:
		new_top_row[x].walls.top = false

	__build_maze(new_maze, old_maze)

	emit_signal('maze_extended')


func __build_maze(new_maze, old_maze):
	var new_maze_visited = UTIL.filled_matrix(MAZE_WIDTH, MAZE_HEIGHT_PER_CHUNK, 2)
	var walls_wip: Array = []
	
	# set goal at bottom edge:
	var goal_x = UTIL.rng.randi_range(1, MAZE_WIDTH-2)
	new_maze[MAZE_HEIGHT_PER_CHUNK-1][goal_x].tile = { type_id = ID.GOAL }
	new_maze[MAZE_HEIGHT_PER_CHUNK-1][goal_x].walls.top = false
	new_maze[MAZE_HEIGHT_PER_CHUNK-1][goal_x].walls.bottom = false
	new_maze[MAZE_HEIGHT_PER_CHUNK-2][goal_x].walls.bottom = false
	new_maze_visited[MAZE_HEIGHT_PER_CHUNK-1][goal_x] = 0
	walls_wip.append({ x = goal_x, y = MAZE_HEIGHT_PER_CHUNK-1, dir = 'left' })
	walls_wip.append({ x = goal_x, y = MAZE_HEIGHT_PER_CHUNK-1, dir = 'right'})
	goal_tile = Vector2(goal_x, new_maze.size() + old_maze.size() - 1)

	while len(walls_wip) > 0:
		# get current wall and tile:
		var wall_i = UTIL.rng.randi_range(0, len(walls_wip)-1)
		var current_wall = walls_wip[wall_i]
		walls_wip.remove(wall_i)
		var current_tile = new_maze[current_wall.y][current_wall.x]

		# get referenced tile:
		var neighbour_tile_y = current_wall.y
		var neighbour_tile_x = current_wall.x
		var inverse_dir = 'top'
		var untouched_dirs = []
		if current_wall.dir == 'top':
			neighbour_tile_y -=1
			inverse_dir = 'bottom'
			untouched_dirs = ['left', 'right']
		elif current_wall.dir == 'right':
			neighbour_tile_x += 1
			inverse_dir = 'left'
			untouched_dirs = ['top', 'bottom']
		elif current_wall.dir == 'bottom':
			neighbour_tile_y += 1
			inverse_dir = 'top'
			untouched_dirs = ['left', 'right']
		elif current_wall.dir == 'left':
			neighbour_tile_x -= 1
			inverse_dir = 'right'
			untouched_dirs = ['top', 'bottom']

		# make sure its not out of bounds:
		if (
			neighbour_tile_x < 0 or neighbour_tile_x > MAZE_WIDTH - 1
		) or (
			neighbour_tile_y < 0 or neighbour_tile_y > MAZE_HEIGHT_PER_CHUNK - 1
		):
			continue

		# already visited? whack:
		if new_maze_visited[neighbour_tile_y][neighbour_tile_x] <= 0:
			continue
		
		# cool, we check this tile now:
		var neighbour_tile = new_maze[neighbour_tile_y][neighbour_tile_x]

		# to add more variety, we randomly decide if the tile should be checked again:
		new_maze_visited[neighbour_tile_y][neighbour_tile_x] -= UTIL.rng.randi_range(1, 2)

		# tear down walls:
		var remaining_walls_for_neighbour = 0
		for dir in ['top', 'right', 'bottom', 'left']:
			if neighbour_tile.walls[dir]:
				remaining_walls_for_neighbour += 1
		if remaining_walls_for_neighbour > 2:
			# remove if the tile still has walls afterwards
			current_tile.walls[current_wall.dir] = false
			neighbour_tile.walls[inverse_dir] = false
		else:
			# else we leave the tile alone to avoid big empty spaces
			continue

		# write up the other walls to get them later:
		for dir in untouched_dirs:
			if (
				dir == 'top' and neighbour_tile_y == 0
			) or (
				dir == 'right' and neighbour_tile_x == MAZE_WIDTH - 1
			) or (
				dir == 'bottom' and neighbour_tile_y == MAZE_HEIGHT_PER_CHUNK - 1
			) or (
				dir == 'left' and neighbour_tile_x == 0
			):
				continue
			walls_wip.append({ y = neighbour_tile_y, x = neighbour_tile_x, dir = dir })

	# all done
	old_maze.append_array(new_maze)
	maze = old_maze.duplicate(true)


func _on_adventurer_moving(new_tile: Vector2, adventurer: Adventurer):
	for adv in adventurers:
		if adv.ref == adventurer:
			adv.tile = new_tile
			return
	adventurers.append({
		'ref': adventurer,
		'tile': new_tile
	})


func _on_adventurer_joined_group(adventurer: Adventurer):
	var delete_index = -1
	for i in adventurers.size():
		if adventurers[i] == adventurer:
			delete_index = i
			break
	if delete_index != -1:
		adventurers.remove(delete_index)


func _on_adventurer_died(class_ids: Array):
	change_adventurers_alive(-class_ids.size())
	for class_id in class_ids:
		add_soul()
		change_life_force_count(DATA.get_life_force(class_id))


func get_closest_adventurer(tile: Vector2) -> Dictionary:
	var closest = { 'distance': -1, 'tile': Vector2.ZERO, 'adv': null }
	var deletable_refs = []
	for adv in adventurers:
		if not is_instance_valid(adv.ref) or adv.ref.is_queued_for_deletion():
			deletable_refs.append(adv)
			continue
		var dist = (adv.tile - tile).length_squared()
		if dist < closest.distance or closest.distance == -1:
			closest.distance = dist
			closest.tile = adv.tile
			closest.adv = adv.ref
	for ref in deletable_refs:
		adventurers.erase(ref)
	return closest


func get_dist_start_goal() -> int:
	return (start_tile - goal_tile).length_squared()


func update_monster_position(old_tile: Vector2, new_tile: Vector2, monster):
	maze[old_tile.y][old_tile.x].monster = null
	maze[new_tile.y][new_tile.x].monster = monster

	emit_signal('monster_moved', old_tile, new_tile, monster)


func add_soul() -> int:
	souls += 1
	emit_signal('souls_count_changed', souls)
	return souls


func get_souls_count() -> int:
	return souls


func change_life_force_count(change: int) -> int:
	life_force += change
	emit_signal('life_force_count_changed', life_force)
	return life_force


func get_life_force_count() -> int:
	return life_force


func get_random_class() -> int:
	var available_classes = DATA.get_classes()
	if souls <= MIN_SOULS_FOR_HARDER_CLASSES:
		available_classes.erase(ID.TEAMLEADER)
	return available_classes[UTIL.rng.randi() % available_classes.size()]


func change_adventurers_alive(amount: int):
	adventurers_alive += amount
	if adventurers_alive == 0:
		emit_signal('no_adventurer_alive')


func __advance_timer():
	yield(get_tree().create_timer(1.0), "timeout")

	playtime += 1
	emit_signal('playtime_advanced', playtime)

	__advance_timer()
