extends Node2D


var debugSignals = false


# Signals:
signal adventurer_moving(new_tile, adventurer)
signal adventurer_joined_group(adventurer)
signal adventurer_reached_goal()
signal adventurer_died(class_ids) # Array[int]
signal start_entity_placing(id, data)
signal abort_entity_placing()
signal start_tile_deleting()
signal abort_tile_deleting()
signal non_ui_mouse_click()
signal spawn_new_adventurer(id)

func _ready():
	if debugSignals:
		for s in self.get_signal_list():
			if s.args.empty():
				connect(s.name, self, "__on_signal_no_args", [s.name])
			elif s.args.size() == 1:
				connect(s.name, self, "__on_signal_one_args", [s.name])
			elif s.args.size() == 2:
				connect(s.name, self, "__on_signal_two_args", [s.name])
			else:
				push_error('[EventManager]! %s name has more than 2 args, debug not set up!' % s.name)


func __on_signal_no_args(name):
	print('[EventManager]: got signal %s' % name)


func __on_signal_one_args(arg, name):
	print('[EventManager]: got signal %s with arg %s' % [name, arg])


func __on_signal_two_args(arg1, arg2, name):
	print('[EventManager]: got signal %s with args %s , %s' % [name, arg1, arg2])
