extends Control


func _ready():
	$"MarginContainer3/CenterContainer/StartButton".connect(
		'pressed', self, '__on_start'
	)

	var label = $"MarginContainer2/VBoxContainer/Label"
	var names = [
		'SKNANA',
		'Momo_Hunter',
		'LauraWilkinson',
		'LucaVazz',
	]
	names.shuffle()
	label.set_text('Game by: %s, %s, %s and %s'%names)



func __on_start():
	SCENE.goto_scene('story')
