# Edges:
```
edge_< position >(_< adjacent tile >)(_join)
       t/r/b/l       r/l
      ^ which edge of the map they define
                    ^ set if there is a tile on that edge, only for position = t / b
                                     ^ set if there is a direct wall on that edge
```

# Edge corners:
```
corner_< position >
         ^ (t/b)(l/r)
```

# Inside wall:
```
wall_< adjacent tiles >
       ^ (t)(r)(b)(l)
```
